package harshsinghchandrawat.com.customergurutasks;

/**
 * Created by HP on 23-02-2018.
 */

public class Model {
    private int count;
    private String value;


    public int getCount() {
        return count;
    }

    public String getValue() {
        return value;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
