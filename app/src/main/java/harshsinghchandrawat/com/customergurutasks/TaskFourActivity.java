package harshsinghchandrawat.com.customergurutasks;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class TaskFourActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mName, mPhone, mEmail;
    private Button mSubmit;
    private Spinner mCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_four);
        getSupportActionBar().hide();

        mName = (EditText) findViewById(R.id.name);
        mPhone = (EditText) findViewById(R.id.phone);
        mEmail = (EditText) findViewById(R.id.email);
        mCity = (Spinner) findViewById(R.id.city);
        mSubmit = (Button) findViewById(R.id.Submit_button);
        mSubmit.setOnClickListener(this);
        String[] spinnerItems = new String[]{"Select one","Delhi", "Mumbai", "kolkata", "Chennai", "Bangalore"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spinnerItems);
        mCity.setAdapter(adapter);
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validate() {
        String name = mName.getText().toString().trim();
        String phone = mPhone.getText().toString().trim();
        String email = mEmail.getText().toString().trim();

        if (name.isEmpty()) {
            mName.setError("Invalid name");
            requestFocus(mName);
            return false;
        }


        if (phone.isEmpty() || phone.length() < 10 || phone.length() < 10) {
            mPhone.setError("Invalid phone");
            requestFocus(mPhone);
            return false;
        }

        if ((email.isEmpty() || !isValidEmail(email))) {
            mEmail.setError("Invalid Email");
            requestFocus(mEmail);
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.Submit_button:
                if (!validate()) {
                    return;
                } else {
                    Toast.makeText(this, "Submitted", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
