package harshsinghchandrawat.com.customergurutasks;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

public class TaskTwoActivity extends AppCompatActivity {
    ArrayList<String> list = new ArrayList<String>();
    ArrayList<Model> listMains = new ArrayList<Model>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_two);

        list.add("AAA");
        list.add("AAA");
        list.add("BBB");
        list.add("BBB");
        list.add("CC");
        list.add("CC");
        list.add("XXX");
        list.add("XXX");
        list.add("PP");
        list.add("QQ");
        list.add("XXX");
        list.add("XXX");
        sortData();

    }

    public void listSort() {
        if (listMains.size() > 0) {
            Collections.sort(listMains, new Comparator<Model>() {
                @Override
                public int compare(Model p1, Model p2) {
                    int c1 = p1.getCount();
                    int c2 = p2.getCount();
                    return c2 - c1;
                }
            });
        }
    }

    private void sortData() {
        Set<String> items = new HashSet<String>(list);
        for (String value : items) {
            Model model = new Model();
            int count = Collections.frequency(list, value);
            model.setValue(value);
            model.setCount(count);
            listMains.add(model);
            //sfsf
        }
        listSort();

        for (Model model: listMains) {
            Toast.makeText(this, model.getValue() + ", " + model.getCount(), Toast.LENGTH_SHORT).show();
            Log.d("Task_Two", model.getValue() + ", " + model.getCount());
        }

    }
}
