package harshsinghchandrawat.com.customergurutasks;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TaskOneActivity extends AppCompatActivity implements View.OnClickListener {

    static final int FROM = 1000;
    static final int TO = 2000;
    static private int mChooseEditF;
    static EditText profile_dob;
    private ImageView Choose_dob;
    static EditText profile_dob1;
    private ImageView Choose_dob1;
    static Context mContext;
    private Button mSubmit;
    private TextView mResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_one);
        profile_dob = (EditText) findViewById(R.id.profile_dob);
        Choose_dob = (ImageView) findViewById(R.id.choose_dob);
        profile_dob1 = (EditText) findViewById(R.id.profile_dob1);
        profile_dob.setEnabled(false);
        profile_dob1.setEnabled(false);
        Choose_dob1 = (ImageView) findViewById(R.id.choose_dob1);
        mSubmit = (Button) findViewById(R.id.submit);
        mResult = (TextView) findViewById(R.id.result);


        Choose_dob.setOnClickListener(this);
        mSubmit.setOnClickListener(this);
        Choose_dob1.setOnClickListener(this);
        mContext = TaskOneActivity.this;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.choose_dob:
                mChooseEditF = TO;
                showDatePickerDialog();
                break;

            case R.id.choose_dob1:
                mChooseEditF = FROM;
                showDatePickerDialog();
                break;

            case R.id.submit:
                String fromDate = profile_dob1.getText().toString().trim();
                String toDate = profile_dob.getText().toString().trim();
                if (!toDate.equals(null) && !fromDate.equals(null) && !toDate.isEmpty() && !fromDate.isEmpty()) {
                    String firstDate = getToDate(fromDate);
                    String lastDate = getFromDate(toDate);
                    Date last = null;
                    try {
                        last = new SimpleDateFormat("dd/MM/yyyy").parse(lastDate);
                    } catch (ParseException e) {
                    }
                    try {
                        if (new SimpleDateFormat("dd/MM/yyyy").parse(firstDate).before(last)) {
                            if (new SimpleDateFormat("dd/MM/yyyy").parse(lastDate).before(new Date())) {
                                mResult.setText("Result : " + firstDate + " - " + lastDate);
                            } else {
                                String date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
                                mResult.setText("Result : " + firstDate + " - " + date);
                            }
                        } else {
                            mResult.setText("Invalid Selection");
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();

                    }
                } else {
                    mResult.setText("Field is empty");
                }
                break;

        }
    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private int mCurrentday, mCurrentyear, mCurrentmonth;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            mCurrentyear = c.get(Calendar.YEAR);
            mCurrentmonth = c.get(Calendar.MONTH);
            mCurrentday = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, mCurrentyear, mCurrentmonth, mCurrentday);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            int Month = month + 1;
            String date = day + "/" + Month + "/" + year;
            try {
                if (new SimpleDateFormat("dd/MM/yyyy").parse(date).before(new Date())) {
                    switch (mChooseEditF) {
                        case FROM:
                            profile_dob1.setText(date);
                            break;

                        case TO:
                            profile_dob.setText(date);
                            break;
                    }
                } else {
                    Toast.makeText(mContext, R.string.invalidate, Toast.LENGTH_SHORT).show();
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }

    private String getToDate(String toDate) {
        String lastFullDate;
        Date convertedDate = null;
        try {
            convertedDate = new SimpleDateFormat("dd/MM/yyyy").parse(toDate);
        } catch (ParseException e) {
        }
        Calendar c = Calendar.getInstance();
        c.setTime(convertedDate);
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        int lastDate = c.getActualMinimum(Calendar.DAY_OF_MONTH);
        String[] parts = toDate.split("/", 2);
        String string2 = parts[1];
        lastFullDate = String.valueOf(lastDate) + "/" + string2;
        return lastFullDate;
    }

    public String getFromDate(String fromDate) {
        String firstFullDate;
        Date convertedDate = null;
        try {
            convertedDate = new SimpleDateFormat("dd/MM/yyyy").parse(fromDate);
        } catch (ParseException e) {
        }
        Calendar c = Calendar.getInstance();
        c.setTime(convertedDate);
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        int firstDate = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        String[] parts = fromDate.split("/", 2);
        String string2 = parts[1];
        firstFullDate = String.valueOf(firstDate) + "/" + string2;
        return firstFullDate;
    }

    public void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
}
