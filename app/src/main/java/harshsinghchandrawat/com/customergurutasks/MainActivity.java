package harshsinghchandrawat.com.customergurutasks;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mTask1, mTask2, mTask3, mTask4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        mTask1 = (Button) findViewById(R.id.task1);
        mTask2 = (Button) findViewById(R.id.task2);
        mTask3 = (Button) findViewById(R.id.task3);
        mTask4 = (Button) findViewById(R.id.task4);

        mTask1.setOnClickListener(this);
        mTask2.setOnClickListener(this);
        mTask3.setOnClickListener(this);
        mTask4.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.task1:
                
                Intent intent = new Intent(this, TaskOneActivity.class);
                startActivity(intent);
                break;

            case R.id.task2:
                Intent intent2 = new Intent(this, TaskTwoActivity.class);
                startActivity(intent2);
                break;

            case R.id.task3:
                //Not done this task
                break;

            case R.id.task4:
                Intent intent4 = new Intent(this, TaskFourActivity.class);
                startActivity(intent4);
                break;
        }
    }
}
